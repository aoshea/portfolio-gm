import {getPostData} from '@/lib/posts';
type Post = {
	id: number;
	slug: string;
}

export async function generateStaticParams() {
	const res = await fetch('https://www.ziggawords.com/wp-json/wp/v2/posts');
	const posts: Post[] = await res.json(); 
	return posts.map((post) => ({
		slug: post.slug,
	}))
}

export default async function Page({ params }: { params: { slug: string }}) {
	const postData = await getPostData(params.slug);
	return <div>My post {params.slug} title: {postData} done.</div>
}


