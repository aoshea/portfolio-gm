import styles from "./page.module.css";

export default function Post() {
	return (
		<article className={styles.article}>
			<div className={styles.description}>
				<p>
					Post description
				</p>
			</div>
		</article>
	);
}

