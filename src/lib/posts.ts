type Post = {
	id: number;
	slug: string;
	title: { rendered: string }
};

export async function getPostData(slug: string) {
	const res = await fetch('https://www.ziggawords.com/wp-json/wp/v2/posts?slug=' + slug);
	const post: Post[] = await res.json();
	return post[0].title.rendered;
}
